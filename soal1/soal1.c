#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <json-c/json.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#define HOME "/home/sarah"


void download_drive(char *id, char *save){
    char *link = malloc(100);
    strcpy(link, "https://docs.google.com/uc?export=download&id=");
    strcat(link, id);
   
    pid_t pid = fork();
    int status;
    if (pid == 0){
        char *argv[] = {"wget", "--quiet", "--no-check-certificate", link, "-O", save, NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    wait(&status);
}

 void create_dir(char *dirname){
     pid_t pid = fork();
    int status;
    if (pid == 0){
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}

void extract(char *zippath, char *extractpath){
    pid_t pid = fork();
    int status;
    if (pid == 0){
        char *argv[] = {"unzip", "-q", zippath, "-d", extractpath, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    wait(&status);
}


void init(){
    download_drive("1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", HOME "/weapons.zip");
    download_drive("1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", HOME "/characters.zip");

    extract(HOME "/weapons.zip", HOME);
    extract(HOME "/characters.zip", HOME);
}

int main(){
    pid_t pid, sid;

    pid = fork();

    if (pid<0){
        exit(EXIT_FAILURE);
    }

    umask(0);

    sid = setsid();
    if (sid<0){
        exit(EXIT_FAILURE);
    }

    if((chdir("/")) < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

init();

return 0;
}