#include <stdio.h>
#include <wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>

void buat_folder(pid_t child_id){
    int status; 
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/william/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }
    while((wait(&status)) > 0);
    char *argv[] = {"unzip", "/home/william/sisop/modul2/drakor.zip", NULL};
    execv("/bin/unzip", argv);   
}
void delete_folder(pid_t child_id){
    char src[PATH_MAX], now[PATH_MAX];
    getsrc(src, PATH_MAX);
    DIR *dir;
    struct dirent *dp;
    int status;
    strcpy(now, src);
    dir = opendir(now);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            strcpy(src, now);
            strcat(src, "/");
            strcat(src, dp->d_name);
            if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "sisop2soal2") != 0){            
                if(child_id <1 && child_id >-1){
                    char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                    printf("%s\n", dp->d_name);
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
                child_id = fork();
            }
        }
    }
}
int main (){
    int status, statdir;
    pid_t child_id;
    char cwd[PATH_MAX];
    child_id = fork();
 
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir(getcwd(cwd, PATH_MAX))) < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id <1 && child_id>-1 ){
        buat_folder(fork());
    }while((wait(&status)) > 0);

    pid_t chld = fork();
        if(child_id <1 && child_id>-1){
            if (chld < 0) exit(EXIT_FAILURE);
            delete_folder(fork());
        }else{
            while((wait(&statdir)) > 0);
        }
}