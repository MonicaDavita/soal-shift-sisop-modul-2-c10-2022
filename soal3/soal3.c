#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>

char animals[256][256], darat[256][256], air[256][256];
int status;

int listAnimals (char arr[][256], char route[]){
    DIR *d;
    struct dirent *dir;
    int index, count = 0;
    d = opendir(route);

    if (d){
        while ((dir = readdir(d)) != NULL){
           if((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0)){
            strcpy(arr[count], dir->d_name);
            count++;
            }else continue;
        }
        closedir(d);
    }
return count;
}

void createDir(char route[]){
    if(0 == fork()) execlp("mkdir", "mkdir", "-p", route, NULL);
    while ((wait(&status)) > 0);
}

int main(){

    // Create directory darat and air in modul2 
    createDir("/home/monica/modul2/darat");
    sleep(3);
    createDir("/home/monica/modul2/air");

    //unzip folder animals
    if(0 == fork()) execlp("unzip", "unzip", "-d", "/home/monica/modul2", "/home/monica/animal.zip", NULL);
    while((wait(&status)) > 0);

    //Organizing animals
    int length = listAnimals(animals, "./modul2/animal");

    for (int i=0; i < length; i++){
        char src[50] = "/home/monica/modul2/animal/";
        if(strstr(animals[i], "darat")){
            strcat(src, animals[i]);
            char destDarat[50] = "/home/monica/modul2/darat";
            if(0 == fork()) execlp("mv", "mv", src, destDarat, NULL);
            else while ((wait(&status)) > 0);
        }else if(strstr(animals[i], "air")){
            strcat(src, animals[i]);
            char destAir[50] = "/home/monica/modul2/air";
            if(0 == fork()) execlp("mv", "mv", src, destAir, NULL);
            else while ((wait(&status)) > 0);
        }else{
            strcat(src, animals[i]);
            if(0 == fork()) execlp("rm", "rm", "--force", src, NULL);
            while ((wait(&status)) > 0);
        }
    }
    
    //Removing bird files in animals folder
    int lenDarat = listAnimals(darat, "./modul2/darat");
    for(int i=0; i < lenDarat; i++){
        char srcBird[50] = "/home/monica/modul2/darat/";
        if(strstr(darat[i], "bird")){
            strcat(srcBird, darat[i]);
            if(0 == fork()) execlp("rm", "rm", "--force", srcBird, NULL);
            while ((wait(&status)) > 0);
        }
    }

    //Making file list.txt
    if(0 == fork()) execlp("touch", "touch", "/home/monica/modul2/air/list.txt", NULL);

    //Write into the list.txt
    FILE *f;
    f = fopen("/home/monica/modul2/air/list.txt", "w");
    if(f == NULL){
        printf("Error! File not found\n");
        exit(1);
    }

    //Writing the filename according to the format
    int lenAir = listAnimals(air, "./modul2/air");

    for (int k=0; k < lenAir; k++){

        char pms[4] = "";
        char filename[60]="";

        //Get the UID and input the final filename to the list.txt
        struct stat message;
        struct passwd *pw;
        int m = stat("/home/monica/modul2/air", &message);
        if(m == -1){
            fprintf(stderr, "Error! File not found\n");
            exit(1);
        }
        else pw = getpwuid(message.st_uid);

        //Get the permission
        struct stat fp;
        int n = stat("/home/monica/modul2/air", &fp);
        if(n == -1){
            fprintf(stderr, "Error! An error occured.");
            exit(1);
        }else{
            if(fp.st_mode & S_IRUSR) strcat(pms, "r");
            if(fp.st_mode & S_IWUSR) strcat(pms, "w");
            if(fp.st_mode & S_IXUSR) strcat(pms, "x");
        }
        
        //Concat the final filename
        strcat(filename, pw->pw_name);
        strcat(filename, "_");
        strcat(filename, pms);
        strcat(filename, "_");
        strcat(filename, air[k]);
        fprintf(f, "%s\n", filename);
    }

    fclose(f);

}