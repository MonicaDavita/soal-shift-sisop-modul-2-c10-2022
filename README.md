# soal-shift-sisop-modul-2-C10-2022
Pembahasan soal praktikum Modul 2 Sistem Operasi 2022

**Anggota Kelompok**:

- Sarah Alissa Putri - 5025201272
- Monica Narda Davita - 5025201009
- William Zefanya Maranatha - 5025201167

---

# Daftar Isi
- [Soal 1](#soal-1)
    
- [Soal 2](#soal-2)
    
- [Soal 3](#soal-3)
    - [Soal 3.a](#soal-3a)
    - [Soal 3.b](#soal-3b)
    - [Soal 3.c](#soal-3c)
    - [Soal 3.d](#soal-3d)
    - [Soal 3.e](#soal-3e)

## Soal 1
### Soal 1.a
**Deskripsi:**\
Download file weapons dan characters dari link yang sudah tertera di soal, kemudian ekstrak file tersebut. Masukkan ke folder `gacha_gacha`

Pertama, buat fungsi untuk mendownload file dari google drive. Dengan *adalah id file pada gdrive dan *save adalah nama file yang akan disimpan. Gunakan `char *link = malloc(100);` untuk mengalokasikan variabel untuk menyimpan link google drive. `strcpy(link, "https://docs.google.com/uc?export=download&id=");` untuk menyalin placeholder link gdrive, dan ` strcat(link, id);` untuk menambah id ke variabel link.
```ruby
void download_drive(char *id, char *save){
    char *link = malloc(100);
    strcpy(link, "https://docs.google.com/uc?export=download&id=");
    strcat(link, id);
```
Spawn child process
```ruby
pid_t pid = fork();
    int status;
    if (pid == 0)
```
Jika process ini merupakan hasil fork (child process) maka jalankan perintah wget untuk mendownload file ke save. Kemudian tunggu hingga selesai.
```ruby
{
    char *argv[] = {"wget", "--quiet", "--no-check-certificate", link, "-O", save, NULL};
    execv("/usr/bin/wget", argv);
    exit(0);
}
    wait(&status);
```
Selanjutnya, buat fungsi untuk membuat folder baru. Jika proses ini merupakan child process maka jalankan perintah mkdir untuk membuat folder dan tunggu hingga selesai.
```ruby
void create_dir(char *dirname)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}
```
Buat fungsi extract untuk mengekstrak file zip dengan dua parameter, zippath adalah path file zip yang ingin diekstrak dan extractpath adalah path tujuan ekstraksi.
```ruby
void extract(char *zippath, char *extractpath)
{
    // Spawn child process
    pid_t pid = fork();
    int status;
    if (pid == 0)
```
Lakukan ekstraksi menggunakan perintah unzip dan tunggu hingga selesai
```ruby
    {
    char *argv[] = {"unzip", "-q", zippath, "-d", extractpath, NULL};
    execv("/usr/bin/unzip", argv);
    exit(0);
    }
    wait(&status);
}
```
Buat fungsi init untuk insialisasi sebelum melakukan simulasi gacha
```ruby
    download_drive("1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", HOME "/weapons.zip");
    download_drive("1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", HOME "/characters.zip");

    extract(HOME "/weapons.zip", HOME);
    extract(HOME "/characters.zip", HOME);
```
## Soal 2
### Soal 2.a
**Deskripsi:**\
Mengekstrak file zip ke dalam folder “/home/[user]/shift2/drakor”. Setelah itu kita akan memfilter yang mana yang merupakan file penting atau folder tidak penting. Jika tidak penting maka akan dihapus.
```ruby
int status, statdir;
    pid_t child_id;
    char cwd[PATH_MAX];
    child_id = fork();
 
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir(getcwd(cwd, PATH_MAX))) < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0 ){
        buat_folder(fork());
    }while((wait(&status)) > 0);
```
Pertama kali di main kita membuat inisiasi untuk memulai proses, setelah itu child_id akan dicek jika child_id nya ==0 maka akan memanggil fungsi buat_folder 
```ruby
void buat_folder(pid_t child_id){
    int status; 
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/william/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }
    while((wait(&status)) > 0);
    char *argv[] = {"unzip", "/home/william/sisop/modul2/drakor.zip", NULL};
    execv("/bin/unzip", argv);   
}
```
Di fungsi buat_folder ini terjadi proses untuk membuat directory dengan menggunakan mkdir yang kita buat alamat foldernya di /home/william/shift2/drakor setelah dieksekusi maka proses akan menunggu dan langsung mengunzip file drakor.zip yang kita arahkan di alamat folder /home/william/sisop/modul2/drakor.zip.
```ruby
 pid_t chld = fork();
        if(child_id ==0){
            if (chld < 0) exit(EXIT_FAILURE);
            delete_folder(fork());
        }else{
            while((wait(&statdir)) > 0);
```
Setelah itu masih dicek child_id nya jika sama dengan 0 maka akan memanggil fungsi delete_folder
```ruby
void delete_folder(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char current[PATH_MAX];
    int status;
 
    // get current location
    strcpy(current, cwd);
    dir = opendir(current);
 ``` 
 Pertama kali kita mencari lokasi dari folder yang akan kita delete dengan cara mengcopy sting dari cwd menuju current dan memasukkan dir dengan current. 
```ruby
 while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "soal2") != 0){            
                if(child_id == 0){
                    printf("%s\n", dp->d_name);
 
                    char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
                child_id = fork();
            }
        }
    }
}
```
Kita buat perulangan untuk mengecek file satu-satu, lalu masuk ke dalam if nya dimana akan di cek dengan string compare jika ada nama file yang bernama . , .. , soal2 maka if tidak akan berjalan. Lalu dicek lagi child_id nya jika sama dengan 0 maka akan langsung dihapus dengan rf (remove folder)setelah itu akan ditunggu jika sudah selesai akan kembali ke main. 

## Soal 3
**[Source Code Soal 3](https://gitlab.com/MonicaDavita/soal-shift-sisop-modul-2-c10-2022/-/blob/main/soal3/soal3.c)**

**Deskripsi:**\
Mengklasifikasikan nama-nama hewan pada folder animal ke dalam folder "darat" dan "air". Nama hewan yang tidak ada termasuk ke dalam jenis hewan darat dan air akan dihapus. Nama hewan pada folder air akan disimpan pada file `list.txt` dan ditulis pula UID beserta jenis permissionnya.

### Soal 3.a
**Deskripsi:**\
Menulis program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.

**Pembahasan:**
- Membuat folder "darat" dan "air" pada route yang sama yaitu "/home/monica/modul2/" dan jeda 3 detik.
```bash
    createDir("/home/monica/modul2/darat");
    sleep(3);
    createDir("/home/monica/modul2/air");
```
- Fungsi createDir berguna untuk membuat direktori yang diinginkan sesuai dengan route-nya. Pada fungsi ini dan seterusnya akan menggunakan `ecexlp`
```bash
    void createDir(char route[]){
    if(0 == fork()) execlp("mkdir", "mkdir", "-p", route, NULL);
    while ((wait(&status)) > 0);
    }
```

**Kendala:**
- Sempat merasa bingung bagaimana parent dan child process-nya. Setelah melihat-lihat modul lagi dan beberapa trial-and-error, permasalahan tersebut dapat terpecahkan.
- Mencari fungsi `exec()` yang tepat lebih tepat untuk kasus seperti ini. Setelah mencoba-coba dengan `execvp()`, dapat diputuskan untuk menggunakan `execlp()` saja karena lebih praktis.

### Soal 3.b
**Deskripsi:**
Program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

**Pembahasan:**
- Dapat dibuat child process untuk melakukan unzip ke route yang sesuai. Pada kode program di bawah ini menggunakan bantuan `execlp()` dan `unzip`.
```bash
if(0 == fork()) execlp("unzip", "unzip", "-d", "/home/monica/modul2", "/home/monica/animal.zip", NULL);
while((wait(&status)) > 0); 
```

**Kendala:**
- Awalnya kebingungan untuk penulisan sintaks yang tepat. Namun setelah menyelami google dengan lebih dalam, permasalahan tersebut dapat terpecahkan.

### Soal 3.c
**Deskripsi:**\
Hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Untuk hewan yang
tidak ada keterangan air atau darat harus dihapus.

**Pembahasan:**
- Menggunakan `for()` loop untuk melakukan traverse satu-per-satu array yang sudah dibuat pada fungsi yang dipanggil. Konsep pada tiap pengondisian selalu sama yaitu melakukan `concat` source file yang sesuai dengan token "darat" maupun "air" dan memindahkannya ke destinasi yang sesuai.
```bash
    int length = listAnimals(animals, "./modul2/animal");

    for (int i=0; i < length; i++){
        char src[50] = "/home/monica/modul2/animal/";
        if(strstr(animals[i], "darat")){
            strcat(src, animals[i]);
            char destDarat[50] = "/home/monica/modul2/darat";
            if(0 == fork()) execlp("mv", "mv", src, destDarat, NULL);
            else while ((wait(&status)) > 0);
        }else if(strstr(animals[i], "air")){
            strcat(src, animals[i]);
            char destAir[50] = "/home/monica/modul2/air";
            if(0 == fork()) execlp("mv", "mv", src, destAir, NULL);
            else while ((wait(&status)) > 0);
        }else{
            strcat(src, animals[i]);
            if(0 == fork()) execlp("rm", "rm", "--force", src, NULL);
            while ((wait(&status)) > 0);
        }
    }
```
- Fungsi `listAnimals()` akan mengembalikan banyaknya file pada suatu folder dan menyimpan nama file pada suatu array (yang diatur sebagai variabel global). Di dalam fungsi tersebut, karena menggunakan `DIR *`, akan ditambah suatu kondisi untuk tidak mengecek nama file ".", "..", "txt", maupun "". Pada fungsi ini, array of char diberi alokasi sebanyak 256 karena angka tersebut dirasa ideal untuk persoalan ini.
```bash
    char animals[256][256], darat[256][256], air[256][256];
    int status;

    int listAnimals (char arr[][256], char route[]){
    DIR *d;
    struct dirent *dir;
    int index, count = 0;
    d = opendir(route);

    if (d){
        while ((dir = readdir(d)) != NULL){
           if((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0)){
            strcpy(arr[count], dir->d_name);
            count++;
            }else continue;
        }
        closedir(d);
    }
    return count;
}
```

**Kendala:**
- Sempat bingung dalam menentukan bagaimana cara melakukan traverse file pada suatu folder dan menyimpanna pada suatu array. Solusi pertama adalah brute force (ditulis satu persatu tanpa fungsi) tetapi setelah melakukan penelitian pada laman google dan stackoverflow serta sedikit modifikasi, masalah tersebut dapat diselesaikan.
- Dalam menghapus file juga ada kendala karena ada beberapa file yang tidak terhapus sehingga memerlukan argumen `--force` agar bisa terhapus. 

### Soal 3.d
**Deskripsi**\
Menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”

**Pembahasan:**
- Konsepnya sama seperti pembahasan soal 3.c, hanya saja token yang digunakan kali ini adalah "bird" dan source filenya berubah.
```bash
 int lenDarat = listAnimals(darat, "./modul2/darat");
    for(int i=0; i < lenDarat; i++){
        char srcBird[50] = "/home/monica/modul2/darat/";
        if(strstr(darat[i], "bird")){
            strcat(srcBird, darat[i]);
            if(0 == fork()) execlp("rm", "rm", "--force", srcBird, NULL);
            while ((wait(&status)) > 0);
        }
    }
```

**Kendala:**
- Tidak ada kendala yang begitu besar karena konsepnya mirip dengan soal 3.c sebelumnya.

### Soal 3.e
**Deskripsi**\
Membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke `list.txt` dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

**Pembahasan:**
- Membuat file `list.txt` ke dalam folder yang diminta melalui child-process
```bash
 if(0 == fork()) execlp("touch", "touch", "/home/monica/modul2/air/list.txt", NULL);
```
- Membuat program yang dapat menulis kedalam file tersebut dengan bantuan `FILE *`
```bash
    FILE *f;
    f = fopen("/home/monica/modul2/air/list.txt", "w");
    if(f == NULL){
        printf("Error! File not found\n");
        exit(1);
    }
```
- Menulis filename yang sesuai dengan cara perulangan dan menuliskannya pada `list.txt`. Tidak lupa untuk menuntup FILE pointer.
```bash
    //Writing the filename according to the format
    int lenAir = listAnimals(air, "./modul2/air");

    for (int k=0; k < lenAir; k++){

        char pms[4] = "";
        char filename[60]="";

        //Get the UID and input the final filename to the list.txt
        struct stat message;
        struct passwd *pw;
        int m = stat("/home/monica/modul2/air", &message);
        if(m == -1){
            fprintf(stderr, "Error! File not found\n");
            exit(1);
        }
        else pw = getpwuid(message.st_uid);

        //Get the permission
        struct stat fp;
        int n = stat("/home/monica/modul2/air", &fp);
        if(n == -1){
            fprintf(stderr, "Error! An error occured.");
            exit(1);
        }else{
            if(fp.st_mode & S_IRUSR) strcat(pms, "r");
            if(fp.st_mode & S_IWUSR) strcat(pms, "w");
            if(fp.st_mode & S_IXUSR) strcat(pms, "x");
        }
        
        //Concat the final filename
        strcat(filename, pw->pw_name);
        strcat(filename, "_");
        strcat(filename, pms);
        strcat(filename, "_");
        strcat(filename, air[k]);
        fprintf(f, "%s\n", filename);
    }

    fclose(f);
```
**Kendala:**
- Kendala utama ada di bagaimana mendapat UID dan Permission file-nya. Sekali lagi harus melihat-lihat modul dan menyelami google.
- Sempat bingung bagaimana menjadi-satukan nama file dengan efektif tanpa menggunakan banyak `strcat()`.

### Bukti Nomor 3
- File animal masih zip dan tidak ada folder modul2
![kosongan](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS2/the_animal_zip_and_no_folder_modul2.jpg)

- File modul2, animal, darat, dan air telah ada
![folderscreated](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS2/animal_darat_air_created.jpg)

- Bukti files pada folder animal telah ter-unzip
![unzip](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS2/unzip.jpg)

- Bukti folder animal telah kosong karena dipindahkan ke folder darat dan air. File frog telah terhapus juga.
![animalempty](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS2/animal_empty.jpg)

- Bukti folder darat kini tidak ada "bird". Terdapat 8 files pada folder darat.
![nobird](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS2/darat_has_no_bird.jpg)

- Bukti file `list.txt` telah terisi dengan nama file pada folder air sesuai dengan keinginan soal.
![airwithlist](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS2/air_with_list_txt.jpg)
